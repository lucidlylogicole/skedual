import configparser
import datetime, time
import os, sys
import subprocess, threading

VERSION='1.2.1'

class Skedual:
    def __init__(self,config_file_path = None):
        '''initialize schedule with optional config file path'''
        if config_file_path != None:
            self.config_file_path = config_file_path
            self.configD = self.getSchedule()
            self.logpath = self.configD['settings']['logpath']

    #---Schedule
    def getSchedule(self):
        '''Get Schedule from config file'''
        
        # Convert Config to Dictionary
        def configToDict(config):
            confD = {}
            for section in config.sections():
                confD[section] = {}
                for key, val in config.items(section):
                    confD[section][key] = val
            return confD
        
        # Read config file
        parser = configparser.ConfigParser()
        parser.read(self.config_file_path)
        return configToDict(parser)

    def run(self):
        '''analyze the schedule to figure out what to run (and run once)'''
        
        # Current Time
        now = datetime.datetime.now()
        c_hour = str(now.hour)
        c_min = str(now.minute)
        c_day = str(now.weekday()+1) # mon=0
        
        # Go through scripts
        for script in self.configD:
            if script != 'settings':
                sD = self.configD[script]
                freq = sD['frequency']
                job_freq = self.configD['settings']['frequency']
                s_hours = ['0']
                s_minutes = ['0']
                if 'hour' in sD: s_hours = sD['hour'].strip().split(',')
                if 'minute' in sD: s_minutes = sD['minute'].strip().split(',')
                run_script = 0
                
                if job_freq == 'hourly':
                    # Hourly Check
                    if freq == 'hourly':
                        run_script = 1
                    elif freq == 'daily' and c_hour in s_hours:
                        # Daily
                        run_script = 1
                    elif freq == 'weekly' and c_day in sD['day'].strip().split(',') and c_hour in s_hours:
                        # Weekly
                        run_script = 1
                else: # job_freq == 'minutely'
                    # Minutely Check
                    if freq == 'minutely':
                        run_script = 1
                    if freq == 'hourly' and c_min in s_minutes:
                        # Hourly
                        run_script = 1
                    elif freq == 'daily' and c_hour in s_hours and c_min in s_minutes:
                        # Daily
                        run_script = 1
                    elif freq == 'weekly' and c_day in sD['day'].strip().split(',') and c_hour in s_hours and c_min in s_minutes:
                        # Weekly
                        run_script = 1
                    
                # Run Script in new Thread
                if run_script:
                    self.runScript(script)

    def runContinuous(self):
        '''continuously run the scheduler'''
        
        print('Starting Scheduler >>>>')
        prev_time = datetime.datetime(2000,1,1)
        freq = self.configD['settings']['frequency']
        
        while 1:
            now = datetime.datetime.now()
            if freq == 'hourly':
                if now.day != prev_time.day or now.hour != prev_time.hour:
                    # Run Hourly
                    prev_time = now
                    self.configD = self.getSchedule()
                    self.run()
                    
                time.sleep(30)
            else:
                # Minutely
                if now.hour != prev_time.hour or now.minute != prev_time.minute:
                    # Run Hourly
                    prev_time = now
                    self.run()
                time.sleep(10)

    def runScript(self,name):
        '''run script by name (once)'''
        
        print('running ',name)
        logpath = self.logpath
        threading.Thread(target=runScript, args=[name,self.configD[name]['cmd'],logpath]).start()


    #---Logging
    def writeSchedule(self):
        '''write a schedule overview html file'''
        
        # Get Style
        style = '''td a {padding:2px 6px;} td,th {text-align:left;} td,th {padding:2px 6px;}'''
        style_path = os.path.join(os.path.dirname(__file__),'schedule_style.css')
        if os.path.exists(style_path):
            with open(style_path,'r') as f:
                style = f.read()
        
        # Header
        html = f'''    <style>{style}</style>
        <table border=1 cellspacing=0>
            <tr class="head"><th>Script Name</th><th>Frequency</th><th>Hour</th><th>Day</th><th>Daily Logs</th></tr>
            <tr class="summary"><th>Summary</th><td>{self.configD['settings']['frequency']}</td><td></td><td></td>\n    <td>'''
        
        # Add Summary Row
        for i in range(1,32):
            html += f'<a href="summary_{i}.log">{i}</a>'
        html += '\n   </td>\n</tr>'
        
        # Load Scripts into Table
        for script in sorted(list(self.configD.keys())):
            if script != 'settings':
                html += "\n<tr>"
                sD = self.configD[script]
                hr = ''
                if 'hour' in sD: hr = sD['hour']
                day = ''
                if 'day' in sD: day = sD['day']
                html += f"\n    <td>{script}</td><td>{sD['frequency']}</td><td>{hr}</td><td>{day}</td>"
                html += '\n    <td>'
                for i in range(1,32):
                    html += f'<a href="details/{script}_{i}.log">{i}</a>'
                
                html += "</td>\n</tr>"
        
        # Write File
        with open(os.path.join(self.logpath,'schedule.html'),'w') as f:
            f.write(html)

    def clearSummary(self,force=0):
        '''Clear Summary Log if not modified today'''
        
        clear_log = force
        current_day = time.strftime('%d')
        summary_path = os.path.join(self.logpath,f'summary_{current_day}.log')
        if os.path.exists(summary_path):
            logtime = datetime.datetime.fromtimestamp(os.path.getmtime(summary_path))
            if logtime.date() != datetime.date.today():
                clear_log=1
        else:
            clear_log=1
        if clear_log:
            with open(summary_path,'w') as f:
                f.write('Summary Log for '+time.strftime('%b %d, %Y')+'\n')
                f.write('---------------------------------------------\n')


#---
#---Script Running Thread
def runScript(name,cmd,logpath):
    '''run a script'''
    
    current_day = time.strftime('%d')
    script_log_path = os.path.join(logpath,'details',f'{name}_{current_day}.log')
    
    # Clear Log if not modified today
    clear_log = 0
    if os.path.exists(script_log_path):
        logtime = datetime.datetime.fromtimestamp(os.path.getmtime(script_log_path))
        if logtime.date() != datetime.date.today():
            clear_log=1
    else:
        clear_log=1
    if clear_log:
        with open(script_log_path,'w') as f:
            f.write(name+' Log for '+time.strftime('%b %d, %Y')+'\n')
            f.write('---------------------------------------------\n')
    
    # Write Log
    def writeLog(txt, status=' '):
        logfile.write(time.strftime('%H:%M:%S')+' '+status+' '+txt+'\n')
        logfile.flush()
    
    # Start to summary
    writeSummary(logpath,f'Start     {name}')
    
    # Start Script
    status = 2
    st = time.time()
    with open(script_log_path,'a') as logfile:
        writeLog('Start')
        proc = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        
        # STDOUT
        with proc.stdout:
            try:
                for line in iter(proc.stdout.readline, b''):
                    writeLog(line.decode("utf-8").rstrip())
            except CalledProcessError as e:
                print(f"{str(e)}")
                try: writeLog('ERROR writing stdout','E')
                except:pass
            
        # STDERR
        with proc.stderr:
            try:
                for line in iter(proc.stderr.readline, b''):
                    writeLog(line.decode("utf-8").rstrip(),'E')
            except CalledProcessError as e:
                print(f"{str(e)}")
                try: writeLog('ERROR writing stderr','E')
                except:pass
        
        # Process Finished
        proc.wait() # Wait for process
        
        # Check for Error
        status = proc.returncode
        if status == 1:
            # Error to summary
            writeSummary(logpath,f'ERROR     {name}')
        
        total_time = time.time()-st
        writeLog(f'Finished ({total_time:0.1f}s)\n')

    # Finish to summary
    writeSummary(logpath,f'Finished  {name} ({total_time:0.1f}s)')

def writeSummary(logpath,txt):
    '''write to the summary log'''
    
    current_day = time.strftime('%d')
    with open(os.path.join(logpath,f'summary_{current_day}.log'),'a') as writeSummary:
        writeSummary.write(time.strftime('%H:%M:%S')+'  '+txt+'\n')

#---
#---Main
def start():
    '''start and setup'''
    
    # Config File Path
    config_file_path = 'schedule.ini'
    if len(sys.argv) > 1:
        arg1 = sys.argv[1]
        if not arg1 in ['-c','-r','-w']:
            config_file_path = arg1
    if not os.path.exists(config_file_path):
        raise Exception('Config file does not exist: '+config_file_path)
    
    # Get skedual object
    skedual = Skedual(config_file_path)
    
    # Clear Summary
    skedual.clearSummary()
    
    # Initialize
    details_path = os.path.join(skedual.logpath,'details')
    if not os.path.exists(details_path):
        os.mkdir(details_path)

    return skedual

#---__main__
if __name__ == '__main__':
    # Run Setup
    skedual = start()

    if '-c' in sys.argv:
        # Run Continuously
        skedual.runContinuous()
    elif '-r' in sys.argv:
        # Run a single Script
        name = sys.argv[sys.argv.index('-r')+1]
        # print('running',name)
        skedual.runScript(name)
    elif '-w' in sys.argv:
        # Write the schedule summary
        skedual.writeSchedule()
    else:
        # Analyze Schedule / Run Now
        skedual.run()