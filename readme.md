# Skedual
A very simple scheduler for running many scripts at various frequencies (daily, hourly, per minute, weekly) and automatically handling logging.

- Specify the script schedule in a simple config file
- Logs the stdout, stderr and a summary to daily files (rotated monthly)
- Can be run by an operating system scheduler (like cron) or started to run continuously on its own
- Python3 is the only dependency

## Install
1. Download this repo
2. Modify the `schedule.ini` file
3. Start skedual using one of the methods described in 'Starting the Schedule'

## The Schedule Config (ini) file
The config file consists of a settings section and then sections for each script to run

### Settings Section

    [settings]
    logpath=logs
    frequency=hourly

- `logpath` - the path to where the logs are stored
- `frequency` - the smallest frequency that the schedule will be checked
    - `hourly` or `minutely` are the only options.
    - if scripts are only going to be run hourly, then choose hourly

### Script Section

    [name of script]
    frequency=hourly
    hour=16
    minute=30
    day=1
    cmd=python3 check_stock_price.py

- `frequency` - the frequency to run the script
    - options: `hourly`,`daily`,`weekly` or `minutely`
- `cmd` - the command to run the script
- `hour` - the hour to run the script (optional)
    - utilized for frequencies daily and weekly
- `minute` - the minute to run the script (optional)
    - this is only valid if the main setting frequency is set to minutely, otherwise this is ignored
    - if not set, the minute is assumed to be 0
- `day` - the day of the week (optional)
    - this is only needed for a weekly frequency
    - `1` = Monday `7` = Sunday

#### Multiple entries
The `hour`, `minute`, and `day` values can contain multiple values with comma separation

Example of multiple values:

    [name of script]
    frequency=hourly
    hour=12,16,20
    minute=0,30
    cmd=


## Starting the Schedule

The default way to use skedual is to have the operating system scheduler call skedual at an hourly or per minute frequency. Skedual will check the schedule against the current time to see if any scripts should be run.

Instead of using the operating system scheduler, skedual can also be started to run continuously on its own.


### Check schedule once and run scripts (default)
Check the schedule and run any scripts (once) whose schedule matches the current time.

    python3 skedual.py

### Run schedule continuously and run scripts
Skedual will run and continuously check the schedule and run scripts as they match the schedule.

    python3 skedual.py -c

### Specify a different config file location than `schedule.ini`
When specifying the ini file, it must be the first argument.

    python3 skedual.py my_schedule.ini

or

    python3 skedual.py my_schedule.ini -c

### Run a single script manually
To run a script outside the schedule, use the `-r` argument followed by the name of the script (use quotes if spaces in the name). This will only run that script once and ignore the schedule.

    python3 skedual.py -r "print_date"

### Setting up to run with cron on Linux

1. If needed, update the python3 and schedule.ini paths in `skedual.sh`

2. Open the crontab

        crontab -e

3. Add the following line (to check the schedule hourly)

        0 * * * * "/pathtoskedual/skedual.sh"
    
    or to run every minute ('minutely')
    
        * * * * * "/pathtoskedual/skedual.sh"
    
    - note: if running every minute, also modify the setting section in `schedule.ini` to be `frequency=minutely`
    
4. Save cron


## Logging
Skedual stores the stdout and stderr of the scripts in logs with timestamps and also keeps a summary of when scripts are started and finished.  The detailed logs are stored by script name and day of the month and that day's script will be written over the next month.

- Detailed logs are stored: `logs/details/<script name>_<day of month>.log`
    - `E` after the time indicates stderr
- Summary logs are stored: `logs/summary_<day of month>.log`
    - the start and finish timestamps are included along with if an error occurred

### Sample of a Detailed Log (`logs/details/print_date_18.log`)

    print_date Log for Dec 18, 2021
    ---------------------------------------------
    14:37:51   Start
    14:37:51   Sat Dec 18 14:37:51 CST 2021
    14:37:51   Finished (0.0s)

    15:37:51   Start
    15:37:51 E date: invalid date ‘e’
    15:37:51   Finished (0.0s)

- the `E` after the time indicates the script output stderr

## schedule.html
Skedual can create a summary file, `logs/schedule.html`, that lists all the scripts with links to their daily logs.

To generate or update (if the schedule changed):

    python3 skedual.py -w

To use a custom style for the summary file, create a file called `schedule_style.css` in the main skedual directory.

## Using a Custom Defined Schedule Format
To use a schedule format other than ini/config, the `Skedual` class can be subclassed and a custom `getSchedule` function can be used.

    import skedual

    class CustomSkedual(skedual.Skedual):
        def __init__(self):
            # Logpath must be specified for other functions
            self.logpath = '/path/to/logs'

        def getSchedule(self):
             # Get Config from api
            import requests
            response = requests.get('https://mywebsite.com/myskedual)
            self.configD = response.json()

    myschedule = CustomSkedual()

    # Check Schedule and run (once)
    myschedule.run()

    # or to run continuously if not using cron
    myschedule.runContinuous()

